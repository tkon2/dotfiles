# Tips and tricks

# Update submodules in all subdirs
# ls | xargs -i sh -c 'cd {} && git status &>/dev/null && git submodule update'


# Reload config
alias src='source ~/.bashrc'

# ls
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'
alias lt='ls -AlFt'

# GIT commands
alias   g='git status'
alias  ga='git add'
alias  gs='git switch'
alias gsm='git switch main'
alias gsb='git switch -'
alias  gr='git restore'
alias  gb='git branch' # Git Branch
alias  gf='git fetch --prune' # Git Fetch
alias  gp='git pull --ff-only --prune' # Git Pull
alias  gu='git push' # Git Upload
alias  gm='git merge'
alias gmm='git merge main'
alias  gl='git log'
alias  gg='git log --graph --oneline'
alias  gd='git diff'
alias  gc='echo Use gs or gr!'
alias gdc='git diff --cached'
alias gra="git for-each-ref --format='%(if:equals=[gone])%(upstream:track)%(then)%(refname:short)%(end)' refs/heads | xargs git branch -D"
alias gsu="git submodule update --init"
alias gsp="git submodule foreach git pull"
alias gdsubdir="find . -mindepth 1 -maxdepth 1 -type d -printf '%p\n' -exec git --git-dir={}/.git --work-tree=$PWD/{} status -s 2>/dev/null \;"
alias gbsubdir="find . -mindepth 1 -maxdepth 1 -type d -printf '\n %p \t ' -exec git --git-dir={}/.git --work-tree=$PWD/{} rev-parse --abbrev-ref HEAD 2>/dev/null \; | column -t"

# npm commands
alias s='BROWSER=none npm-run' # Start
alias npm-bump='npm version --commit-hooks false --git-tag-version false patch'

# react native
alias rn="npx react-native"
alias rnstart='npx react-native start'
alias rnrun='npx react-native run-android'

# Dotnet EF
alias efu='dotnet ef database update'

# OTHER
alias c='code . &'
alias cat='bat'
alias rg='rg --smart-case --path-separator //'
alias adb_reverse="adb devices | awk 'NR>1{print \$1}' | xargs -I _ adb -s _ reverse tcp:5000 tcp:5000;"
alias tldr='tldr -s'


# Functions

gpr() {
  git push --set-upstream origin "$(git branch --show-current)";
}

gssubdir() {
  for D in *; do
    if [ -d "${D}" ] && [ -d "${D}/.git" ]; then
      cd "${D}"
      status="$(git diff --shortstat 2>/dev/null)"
      if ! [ -z "$status" ]
      then
        echo "${D} $status"
      fi
      cd ..
    fi
  done
}

# Enable colour
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto -h --group-directories-first'
    alias dir='dir --color=auto'
    alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# Scripts
alias s='auto_run.sh'
