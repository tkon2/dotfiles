" use Vim mode instead of pure Vi, it must be the first instruction
set nocompatible

" Leader key
let mapleader = " "

" Set tab size, shift size, and tabstop
let indent = 2

" Enable path autocomplete in :open and similar
set wildmode=longest,list,full
set wildmenu

" Split sensibly
set splitbelow
set splitright

" Disable bell
set visualbell
set t_vb=

" Disable clipboard?
set clipboard=exclude:.*

" display settings
set encoding=utf-8 " encoding used for displaying file
set ruler " show the cursor position all the time
set showmatch " highlight matching braces
set showmode " show insert/replace/visual mode
set showcmd " Show partial commands
set scrolloff=2 " Show context before/after scroll
set number relativenumber
set laststatus=2 " Always display the status line, even if only one window is displayed

" write settings
set confirm " confirm :q in case of unsaved changes
set fileencoding=utf-8 " encoding used when saving file
set nobackup " do not keep the backup~ file
set hidden " Allow switching buffers before saving

" edit settings
set backspace=indent,eol,start " backspacing over everything in insert mode
set expandtab " fill tabs with spaces
set nojoinspaces " no extra space after '.' when joining lines
set nostartofline

" Set indent width
let &shiftwidth=indent
let &softtabstop=indent
let &tabstop=indent

" search settings
set hlsearch " highlight search results
set incsearch " do incremental search
set ignorecase " do case insensitive search...
set smartcase " ...unless capital letters are used

" file type specific settings
filetype on " enable file type detection
filetype plugin on " load the plugins for specific file types
filetype indent on " automatically indent code

" Support different comments in git commit messages
" Spesifically semicolons instead of # for comments
au FileType gitcommit syntax match gitcommitComment /^[;].*/

" syntax highlighting
set background=dark " dark background for console
syntax on " enable syntax highlighting

" characters for displaying non-printable characters
set listchars=eol:$,tab:>-,trail:.,nbsp:_,extends:+,precedes:+

" KEYMAPPING

nnoremap <C-s> :w<CR>

" Enable easy copy paste
vnoremap <C-c> "+y

" center view on the search result
noremap n nzz
noremap N Nzz

" Map Y to act like D and C
nnoremap Y y$
nnoremap <leader>c gc

" Map leader v to copy to end of line not including newline char
nnoremap <leader>v vg_

" Map leader V to select whole line not including leading and trailing
" whitespace
nnoremap <leader>V ^vg_

" Map leader , to verticalising a list
nnoremap <leader>, :s/\([,;]\) /\1\r/g<return>:nohlsearch<return>

" Shhh - turn off highlight
nnoremap <leader>s :nohlsearch<return>

" Buffer navigation
nnoremap [b :bp<CR>
nnoremap ]b :bn<CR>

" PLUGINS

" automatic commands
if has('autocmd')
       " don't replace Tabs with spaces when editing makefiles
       autocmd Filetype makefile setlocal noexpandtab

       " delete any trailing whitespaces
       autocmd BufWritePre * :%s/\s\+$//ge
endif

" Install plug if not installed
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.vim/plugged')
    " Make sure you use single quotes
    Plug 'tpope/vim-surround'
    Plug 'tpope/vim-repeat'
    Plug 'tpope/vim-commentary'
    Plug 'neoclide/coc.nvim', {'branch': 'release'}
    Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
    Plug 'ctrlpvim/ctrlp.vim'
    Plug 'machakann/vim-highlightedyank'
call plug#end()

" PLUGIN SETTINGS

" Add RipGrep support
let g:ackprg = 'rg --vimgrep --smart-case'

nnoremap <C-p> :Files<Cr>

" Enable fuzzy find
set rtp+=~/.fzf

" Enable yank highlights
if !exists('##TextYankPost')
  nmap y <Plug>(highlightedyank)
  xmap y <Plug>(highlightedyank)
  omap y <Plug>(highlightedyank)
endif
let g:highlightedyank_highlight_duration = 100

map <space> <leader>
