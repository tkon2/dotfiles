# If not running interactively, don't do anything
case $- in
    *i*) ;;
    *) return;;
esac

# Enable vim mode
set -o vi

# Set default programs
export EDITOR=vim
export PAGER=less

stty -ixon # disable ctrl+s pause output function

set -o vi

# History options
HISTCONTROL=ignoreboth
HISTSIZE=1000
HISTFILESIZE=2000
shopt -s histappend # append to the history file

shopt -s autocd # Turn on auto cd
shopt -s checkwinsize # check window size after cmd

# ** matches all files and zero or more dirs
shopt -s globstar

# Set prompt
PS1="\[\033[01;34m\]\w\[$(tput sgr0)\]\[\033[38;5;202m\] > \[$(tput sgr0)\]"

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"


# Case insensitive tab complete
bind 'set completion-ignore-case on'

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

# Enable actual symlinks in Windows
export MSYS=winsymlinks:nativestrict

# Set up RipGrep for fzf
export FZF_DEFAULT_COMMAND='rg --files . 2>/dev/null'

[ -f ~/.fzf.bash ] && source ~/.fzf.bash

# Windows is really stupid sometimes
export PROMPT_COMMAND=${PROMPT_COMMAND:+"$PROMPT_COMMAND; "}'printf "\e]9;9;%s\e\\" "$(cygpath -w "$PWD")"'

# SET UP CUSTOM SCRIPTS
export PATH=$PATH:~/Scripts

# Alias definitions.
if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

export XDG_CONFIG_HOME=~/.config

# FZF Options
if command -v rg &>/dev/null; then
  export FZF_DEFAULT_COMMAND='rg --files --no-ignore-vcs --hidden'
fi
source /usr/share/doc/fzf/examples/key-bindings.bash
source /usr/share/doc/fzf/examples/completion.bash


# Android setup
export ANDROID_HOME=$HOME/Android/Sdk
export PATH=$PATH:$ANDROID_HOME/emulator
export PATH=$PATH:$ANDROID_HOME/tools
export PATH=$PATH:$ANDROID_HOME/tools/bin
export PATH=$PATH:$ANDROID_HOME/platform-tools

# Yarn setup
export PATH="$HOME/.yarn/bin:$HOME/.config/yarn/global/node_modules/.bin:$PATH"
export PATH="$PATH:$HOME/AppData/Roaming/nvm/v14.18.1/"
export PATH="/usr/local/share/npm/bin:/usr/local/bin:/usr/local/sbin:~/bin:$PATH"
export PATH="$HOME/AppData/Roaming/Python/Python310/Scripts:$PATH"
export PATH="$HOME/flutter/bin:$PATH"

# Dotfiles alias
alias dotfiles="git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME"


export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

export DENO_INSTALL="/home/tkon/.deno"
export PATH="$DENO_INSTALL/bin:$PATH"

date >> ~/dates.deleteme

# bun
export BUN_INSTALL="$HOME/.bun"
export PATH=$BUN_INSTALL/bin:$PATH

export PATH=$PATH:/opt/amdgpu/bin:/opt/amdgpu-pro/bin
