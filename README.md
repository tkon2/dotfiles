## Setup

Get the command alias and set up git:
```
echo ".dotfiles" >> .gitignore

git clone --bare git@gitlab.com:tkon2/dotfiles.git $HOME/.dotfiles

alias dotfiles='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'

echo "alias dotfiles='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'" >> $HOME/.bashrc

dotfiles config --local status.showUntrackedFiles no

dotfiles checkout
```

Now you can run git commands with the dotfiles command, like this:
```sh
dotfiles status

dotfiles add .vimrc

dotfiles commit -m "Added vimrc"

dotfiles add .bashrc

dotfiles commit -m "Added bashrc"

dotfiles push
```